﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using MongoDB.Driver;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Repositories
{
    public class MongoDBRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        private readonly IMongoCollection<T> _collection;

        public MongoDBRepository(IMongoDatabase mongoDatabase)
        {
            _collection = mongoDatabase.GetCollection<T>(typeof(T).Name);
        }

        public async Task AddAsync(T entity) 
            => await _collection.InsertOneAsync(entity);


        public async Task DeleteAsync(T entity) 
            => await _collection.DeleteOneAsync(x => x.Id == entity.Id);


        public async Task<IEnumerable<T>> GetAllAsync() 
            => await _collection.Find(_ => true).ToListAsync();


        public async Task<T> GetByIdAsync(Guid id)
            => await _collection.Find(x => x.Id == id).FirstOrDefaultAsync();


        public async Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate) 
            => await _collection.AsQueryable().Where(predicate).FirstOrDefaultAsync();


        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids) 
            => await _collection.Find(x => ids.Contains(x.Id)).ToListAsync();


        public async Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate) 
            => await _collection.AsQueryable().Where(predicate).ToListAsync();


        public async Task UpdateAsync(T entity) 
            => await _collection.ReplaceOneAsync(x => x.Id == entity.Id, entity);

    }
}
