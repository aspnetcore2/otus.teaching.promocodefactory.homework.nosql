﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public class MongoDBInitializer
        : IDbInitializer
    {
        private readonly IMongoDatabase _mongoDatabase;

        public MongoDBInitializer(IMongoDatabase mongoDatabase)
        {
            _mongoDatabase = mongoDatabase;
        }

        public void InitializeDb()
        {
            ReinitCollection<Preference>(FakeDataFactory.Preferences);
            ReinitCollection<Customer>(FakeDataFactory.Customers);
        }

        private void ReinitCollection<T>(IEnumerable<T> initData)
        {
            IMongoCollection<T> preferenceCollection = _mongoDatabase.GetCollection<T>(nameof(T));
            
            preferenceCollection.DeleteMany(_ => true);
            preferenceCollection.InsertMany(initData);
        }
    }
}
